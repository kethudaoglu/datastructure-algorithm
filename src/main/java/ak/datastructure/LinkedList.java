/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.datastructure;

/**
 *
 * @author alperen
 */


public class LinkedList {

    
 private static class Node {

    Node next;
    int data;

    public Node(int data) {
        this.data = data;
    }

}
    
    Node head;

    
    
    public void append(int data) {
        if (head == null) {
            head = new Node(data);
            return;
        }

        Node current = head;

        while (current.next != null) {
            current = current.next;
        }
        current.next = new Node(data);
    }

    public void preAppend(int data) {
        if (head == null) {
            head = new Node(data);
            return;
        }
        Node newHead = new Node(data);
        newHead.next = head;
        head = newHead;
    }

    public boolean remove(int data) {
        if (head == null) {
            return Boolean.FALSE;
        }
        if (head.data == data) {
            head = head.next;
            return Boolean.TRUE;
        }
        Node current = head;
        while (current.next != null) {
            if (current.next.data == data) {
                current.next = current.next.next;
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
