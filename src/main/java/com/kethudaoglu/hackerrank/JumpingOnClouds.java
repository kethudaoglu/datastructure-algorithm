/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class JumpingOnClouds {
    public static void main(String[] args) {
        int [] data=new int[]{0,0,1,0,0,1,0};
        System.out.println(jumpingOnCloud(data));
    }
    
    public static int jumpingOnCloud(int []cloud){
        int jump=0;
        final int SIZE=cloud.length-1;
        int i=0;
        while (i<SIZE){
            i+=2;
            if (i==cloud.length ) {
                jump++;
                break;
            }
            if (cloud[i]!=0){
                i--;      
            }
            jump++;
        }
        
        
        return jump;
    }
    
}
