/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.graph;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Stack;

/**
 *
 * @author a.kethudaoglu
 */
public class GraphDFS {

    private int V;
    private LinkedList<Integer> adj[];
    public final Stack<Integer> stack=new Stack<>();
   public GraphDFS(int v) {
        this.V = v;
        adj = new LinkedList[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new LinkedList<>();
        }

    }

    public void addEdge(int v, int w){
     adj[v].add(w);
    }
    private void dfsUtil(int v, boolean visited[]){
        visited[v]=true;
        stack.push(v);
        ListIterator<Integer> i = adj[v].listIterator();
        while (i.hasNext()) {
            int n=i.next();
            if (!visited[n]){
                dfsUtil(n,visited);
            }
        }
    }
    public void dfs(int v){
        boolean visited[]=new boolean[V];
        dfsUtil(v, visited);
    }
    
    
}
