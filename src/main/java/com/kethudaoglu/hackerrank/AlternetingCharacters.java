/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class AlternetingCharacters {

    public static void main(String[] args) {
        String data = "AAABBCFFDKKAF";
        System.out.println(data);
        System.out.println(alternetingCharacter(data));
        System.out.println(alternetingCharacterCount(data));
    }
    public static int alternetingCharacterCount(String data){
        final int LOOP_SIZE=data.length()-1;
        int deleteCount=0;
        for (int i = 0; i < LOOP_SIZE; i++) {
            if (data.charAt(i)==data.charAt(i+1)){
                deleteCount++;
            }
            
        }
        return deleteCount;
    }
    public static String alternetingCharacter(String s) {

        Character temp = '-';
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (temp.equals('-')) {
                temp = s.charAt(i);
                result.append(temp);
                continue;
            }
            if (temp.equals(s.charAt(i))) {
                continue;
            }
            temp = s.charAt(i);
            result.append(temp);

        }
        return result.toString();
    }

}
