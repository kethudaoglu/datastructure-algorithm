/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.leetcode.algorithims;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author alperen
 */
public class TwoSum {

    public static void main(String[] args) {
        int data[] = {2, 7, 11, 18};
        int[] r = twoSum(data, 9);
        for (int i : r) {
            System.out.println(i);
        }

    }

    public static int[] twoSum(int args[], int target) {
        int result[] = {0, 0};
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            var key = target - args[i];
            if (map.containsKey(key)) {
                result[0] = map.get(key);
                result[1] = i;
            }
            map.put(args[i], i);
        }
        return result;
    }
}
