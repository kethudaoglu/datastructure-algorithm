/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.Arrays;

/**
 *
 * @author alperen
 */
public class MarkAndToys {
    public static void main(String[] args) {
        int arr[]={10,20,30,70};
        System.out.println(markAndToys(arr, 50));
    }
    
    public static int markAndToys(int arr[],int k){
     Arrays.sort(arr);
     int sum=0;
     int maxToysCount=0;
        for (int i = 0; i < arr.length; i++) {
            sum+=arr[i];
            if (sum<=k){
                maxToysCount++;
            }else {
                break;
            }
            
        }
        return maxToysCount;
    }
}
