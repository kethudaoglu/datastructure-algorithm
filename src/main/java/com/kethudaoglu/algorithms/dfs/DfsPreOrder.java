/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.dfs;

import java.util.Stack;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author a.kethudaoglu
 */
public class DfsPreOrder {

    public static void main(String[] args) {

//        new DfsPreOrder().traverseProOrder(SampleData.getSampleData());
        
        new DfsPreOrder().traversePreOrderWithoutRecursion(SampleData.getSampleData());

    }

    public void traverseProOrder(Node node) {
        if (node != null) {
            visit(node.value);
            traverseProOrder(node.left);
            traverseProOrder(node.right);
        }
    }

    private void visit(int value) {
        System.out.println(value);
    }

    public void traversePreOrderWithoutRecursion(Node root) {
        Stack<Node> stack = new Stack<>();
        Node current = root;
        stack.push(root);
        while (!stack.isEmpty()) {
            current=stack.pop();
            visit(current.value);
            if (current.right!=null){
                stack.push(current.right);
            }
            if (current.left!=null){
                stack.push(current.left);
            }
            
        }

    }
}
