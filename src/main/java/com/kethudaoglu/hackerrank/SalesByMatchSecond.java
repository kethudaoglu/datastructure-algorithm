/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author alperen
 */
public class SalesByMatchSecond {
    public static void main(String[] args) {
        int []ar={10,20,20,10,10,30,50,10,20};
        System.out.println(salesByMatch(ar));
    }
    
    public static int salesByMatch(int data[]){
        Set<Integer> match=new HashSet<>();
        var pair=0;
        var temp=0;
        for (int i = 0; i < data.length; i++) {
            temp=data[i];
           if (!match.contains(temp)){
              match.add(temp);
           }else {
               match.remove(temp);
               pair++;
           }
            
        }
        
      return pair;
    }
    
}
