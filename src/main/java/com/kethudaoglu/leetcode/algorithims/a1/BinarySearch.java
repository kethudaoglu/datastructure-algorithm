/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.leetcode.algorithims.a1;

/**
 *
 * @author alperen
 */
public class BinarySearch {
    public int binarySearch(int nums[],int target){
        int start=0, end=nums.length-1;
        while (start<=end){
            var pivot=start+(end-start)/2;
            
            if (nums[pivot]==target){
                return pivot;
            }
            
            if (nums[pivot]<target){
                start=pivot+1;
            } else {
                end=pivot-1;
            }
            
        }
        return -1;
    }
}
