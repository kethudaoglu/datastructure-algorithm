/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ak.datastructure.graph.beldung;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author alperen
 */
public class Graph {
    
    private Map <Vertex,List<Vertex>> adjVertices=new HashMap<>();
    
    public void addVertex(Vertex v){
        adjVertices.putIfAbsent(v, new ArrayList<>());
    }
    public void removeVertex(Vertex v){
        //komşular arasında varsa sil
        adjVertices.values().stream().forEach(e->e.remove(v));
        //vertex i sil
        adjVertices.remove(v);
    }
    
    //çift yönli kenar
    public void addEdge(Vertex source, Vertex destination){
        adjVertices.get(source).add(destination);
        adjVertices.get(destination).add(source);
    }
    
    public void removeEdge(Vertex v1 , Vertex v2){
       List temp1= adjVertices.get(v1);
       if(temp1!=null){
           temp1.remove(v2);
       }
       List temp2= adjVertices.get(v2);
       if(temp2!=null){
           temp2.remove(v1);
       }
       
    }
    
    public Set<String> depthFirstSearch(String root){
        Set<String> visited=new LinkedHashSet<>();
        Stack<String> stack=new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            String vertex=stack.pop();
            visited.add(vertex);
            for (Vertex vertex1 : adjVertices.get(new Vertex(vertex))) {
                stack.push(vertex1.label);
            }
        }
        
        return visited;
    }
    
    
}
