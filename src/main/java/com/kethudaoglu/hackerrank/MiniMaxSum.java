/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class MiniMaxSum {
    public static void main(String[] args) {
        int arr[]=new int[]{1,3,6,9,6};
        miniMaxSum(arr);
    }
    
    public static void miniMaxSum(int arr[]){
        int min=Integer.MAX_VALUE;
        int max=Integer.MIN_VALUE;
        int sum=0;
        for (int i : arr) {
            sum+=i;
            min=Math.min(min, i);
            max=Math.max(max, i);
        }
        System.out.println("max "+max);
        System.out.println("min "+min);
        System.out.printf("%d %d",sum-max,sum-min);
    }
}
