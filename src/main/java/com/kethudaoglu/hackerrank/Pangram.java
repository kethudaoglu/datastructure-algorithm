/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author alperen
 */
public class Pangram {
    public static void main(String[] args) {
        System.out.println(checkPangram("abcdefghxijklmnoprstuvyzqwokkkkk"));
    }
    
    public static String checkPangram(String data){
        
     var set=new HashSet<Character>();
        for (char c : data.toLowerCase().toCharArray()) {
            if (Character.isLetter(c)) set.add(c);
           
        }
      return set.size()==26?"Pangram":"not pangram";
    
    }
}
