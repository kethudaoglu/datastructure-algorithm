
import com.kethudaoglu.algorithms.sort.BubbleSort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author a.kethudaoglu
 */
public class SomeTest {

    public static void main(String[] args) {
        int a[]={1,4};
        Map<Integer,Integer> map=new HashMap<>();
       
        LinkedList ll=new LinkedList();
        ll.stream().filter(f->true).findFirst().isPresent()
      Map<Integer, List<Integer>> map=new HashMap<>();
      map.put(1, List.of(1,2,3));
      map.put(2,List.of(2,5));
     long value=map.values().stream().flatMap(Collection::stream).distinct().count();
        System.out.println(value);
        
    }

   
    private static long roadsAndLibrarires(int n, int c_lib, int c_road, int [][] cities){
        //to get number of 
        
        n=3;
        
       Graph graph=new Graph();
        for (int i=0;i<n;i++){
            graph.addVertex(i);
        }
        
        for (int i=cities.length;i<cities.length;i++){
            for (int j = 0; j < cities[i].length; j++) {
                graph.addEdge(i, j, true);
                
            }
        }
        
        //we need n-1 road or net library each cities
          //we need n-1 road or net library each cities
        if (c_lib<c_road){
            return graph.getCountOfVertex()*c_lib;
        } else {

          long cost = 0;
          long uniqueRoad=graph.uniqueEdge(true);
          if (uniqueRoad==(n-1)){
          cost=c_lib+c_road*(n-1);
          }else {
          cost=c_lib*(n-uniqueRoad)+uniqueRoad*c_road;
          }
          
//          for (List<Integer> list : graph.map.values()) {
//                int size = list.size();
//                    if (size > 0) {
//                        cost += c_lib;
//                        cost += (size - 1) * c_road;
//                        list.removeIf(i -> true);
//                    }
//            }       
            return cost;     
        }
        
    }
    
    
}

class Graph{
    Map<Integer,List<Integer>> map =new HashMap<>();
    void addVertex(int i){
     map.put(i, new ArrayList<>());
    }
    void addEdge(int source, int destination, boolean bidirectional){
        if (!map.containsKey(source)){
            addVertex(source);
        }
        
        if (!map.containsKey(destination)){
            addVertex(destination);
        }
        
        map.get(source).add(destination);
        if (bidirectional){
            map.get(destination).add(source);
        }
    }
    
    int getCountOfVertex(){
        return map.keySet().size();
    }
    long getCountOfEdge(boolean bidirectional){
        
       long count=map.values().stream().flatMap(Collection::stream).count();
        return bidirectional?count/2:count;
    }
    boolean hasVertex(int source){
        return map.containsKey(source);
    }
   boolean hasEdge(int source, int destination ){
      
       return map.get(source).contains(destination);
   }
   
   long uniqueEdge(boolean bidirectional){
       long count=map.values().stream().flatMap(Collection::stream).distinct().count();
      return bidirectional?count/2:count;
   }
}
