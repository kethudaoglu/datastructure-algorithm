/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

import java.util.stream.IntStream;

/**
 *
 * @author a.kethudaoglu
 */
public class Utils{
       public static  void printArray(int arr[]) {
        IntStream.of(arr).forEach(System.out::println);
    }
}
