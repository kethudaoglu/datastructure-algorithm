
import com.kethudaoglu.algorithms.graph.GraphDFS;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a.kethudaoglu
 */
public class TestGraphDFS {
    @Test
    public void givenGraphDFS_whenDFS_thenSOUT(){
        GraphDFS g = new GraphDFS(4); 
  
        g.addEdge(0, 1); 
        g.addEdge(0, 2);
          g.addEdge(0, 3);
        g.addEdge(1, 2); 
        g.addEdge(2, 0); 
        g.addEdge(2, 3); 
        g.addEdge(3, 3); 
  
        
  
        g.dfs(2); 
        g.stack.stream().forEach(System.out::println);
    }
}
