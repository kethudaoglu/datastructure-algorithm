/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author alperen
 */
public class BalancedBrackets {

    public static void main(String[] args) {
        String data = "{[[]]}";
        System.out.println(checkBlancedBrackets(data));
    }

    public static boolean checkBlancedBrackets(String data) {
        var result = true;
        char[] values = data.toCharArray();
        LinkedList<Character> stack = new LinkedList<>();
        for (char value : values) {
            if (value == '(' || value == '{' || value == '[') {
                stack.push(value);
                continue;
            }

          
            if (value == ')') {
                if (stack.peek().charValue() == '(') {
                    stack.poll();
                } else {
                    return false;
                }

            }
            if (value == '}') {
                if (stack.peek().charValue() == '{') {
                    stack.poll();
                } else {
                    return false;
                }

            }
            if (value == ']') {
                if (stack.peek().charValue() == '[') {
                    stack.poll();
                }else 
                {
                    return false;
                }

           

        }
       

      
    }
          return result;
}
}
