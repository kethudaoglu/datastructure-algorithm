/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.leetcode.algorithims;

/**
 *
 * @author alperen
 */
public class BinarySearch {
    
    public int binarySearch(int nums[], int target){
       return binarySearch(nums,0,nums.length-1,target);
    }
    
   public static int binarySearch(int nums[],int start,int end,int target){
       var pivot=start+(end-start)/2;
       
       if (nums[pivot]==target){
           return pivot;
       }
       
       if (end-start<1){
           return -1;
       }
       
       if (nums[pivot]<target){
           return binarySearch(nums,pivot+1,end,target);
       }else {
           return binarySearch(nums,start,pivot-1,target);
       }
   }
}
