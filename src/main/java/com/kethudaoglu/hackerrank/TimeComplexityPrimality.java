/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class TimeComplexityPrimality {

    public static void main(String[] args) {
            System.out.println(isPrima(8));
    }

    public static boolean isPrima(int data) {
        var sqrt = Math.sqrt(data);
        if (data<1) return false;
        if (data==2) return true;
        if (data%2==0) return false;
        for (int i = 3; i < sqrt; i+=2) {
            if (data % i == 0) {
                return false;
            }

        }
        return true;
    }

}
