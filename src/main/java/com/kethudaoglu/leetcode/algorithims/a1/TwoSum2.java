/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.leetcode.algorithims.a1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author alperen
 */
public class TwoSum2 {
    
    public static void main(String[] args) {
        int numbers[]=new int[5];
        int target=0;   
        int start=0 , end=numbers.length-1;
        int sum=numbers[start]+numbers[end];
        while (sum!=target){
            if (sum<target){
                start++;
                
            }else if (sum>target){
                end--;
                
            }
            sum=numbers[start]+numbers[end];
            
        }
  
    }
    
}
