/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.graph.dijkstra;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author a.kethudaoglu
 */
public class Node {
    String name;
    LinkedList<Node> shortestPath=new LinkedList<>();
    Integer distance=Integer.MAX_VALUE;
    
    Map<Node,Integer> adjacentNodes=new HashMap<>();
    public Node(String name){
        this.name=name;
    }
    
    public void addDestination(Node destination, int distance){
        this.adjacentNodes.put(destination, distance);
    }
    
    
}
