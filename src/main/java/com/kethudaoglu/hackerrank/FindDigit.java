/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class FindDigit {
    public static void main(String[] args) {
        System.out.println(findDigit(12120));
        
    }
    
    public static int findDigit(int value){
        int digit=0;
        int digitCount=0;
        int temp=value;
        while (temp>0){
            digit=temp%10;
            temp/=10;
            if (digit!=0&&value%digit==0){
                digitCount++;
            }
        }
        return digitCount;
    }
}
