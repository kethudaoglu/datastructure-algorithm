/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alperen
 */
public class MinSwapToSort {

    public static void main(String[] args) {
int []arr=new int[]{2,1,5,7,3,4,6};
        System.out.println(minimumSwap(arr));
    }

    public static int minimumSwap(int arr[]) {
        int numberOfTheSwap=0;
        int temp=0;
        for (int i = 0; i < arr.length; i++) {

            if (arr[i] != i + 1) {
             temp=arr[i];
             arr[i]=arr[temp-1];
             arr[temp-1]=temp;
             numberOfTheSwap++;
             i--;
            }

        }
        return numberOfTheSwap;
    }

}
