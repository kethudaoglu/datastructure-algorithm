/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

/**
 *
 * @author a.kethudaoglu
 */
public class MergeSort {
    
    
    public static void mergeSort(int arr[],int n){
        if (n<2){
            return;
        }
        
        int mid=n/2;
        int leftArray[] =new int[mid];
        int rightArray[]=new int[n-mid];
        for (int i = 0; i < mid; i++) {
            leftArray[i]=arr[i];
            
        }
        for (int i = mid; i < n; i++) {
           rightArray[i-mid]=arr[i];
        }
        mergeSort(leftArray, mid);
        mergeSort(rightArray, n-mid);
        merge(arr,leftArray,rightArray,mid,n-mid);
                
    }

    private static void merge(int[] arr, int[] leftArray, int[] rightArray, int leftSize, int rightSize ) {
        int i=0,j=0,k=0;
        
        while (i<leftSize && j<rightSize){
            if (leftArray[i]<=rightArray[j]){
                arr[k++]=leftArray[i++];
            }else {
                arr[k++]=rightArray[j++];
            }
        }
        
        while (i<leftSize){
            arr[k++]=leftArray[i++];
        }
        
        while (j<rightSize){
            arr[k++]=rightArray[j++];
        }
    }
}
