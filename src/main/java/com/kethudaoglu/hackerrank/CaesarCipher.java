/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class CaesarCipher {
    public static void main(String[] args) {
        System.out.println(caeserCipher("alperen", (char)1));
    }
    
    public static String caeserCipher(String data, char k){
        Character c[]=new Character[data.length()];
        StringBuilder sb=new StringBuilder();
        var root='A';
        char ascii=0;
        for (Character character:data.toCharArray()) {
            if (!Character.isLetter(character)){
             sb.append(character);
            }else {
              root=  Character.isUpperCase(character)?'A':'a';
              ascii=(char)((character-root+k)%26+root);
              sb.append(ascii);
            }
        }
        return sb.toString();
    }
}
