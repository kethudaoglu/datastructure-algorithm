/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

/**
 *
 * @author a.kethudaoglu wors-case performance Q(n2) comparisons and swaps
 * avarage-case performance Q(n2)comparisons and swaps best-case Q(n)
 * comparisons, Q(1) swaps worst-case space complexity Q(n) total, Q(1)
 * auxiliary
 */
public class InsertSort {

    public static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];
            int j = i - 1;
            while (j>=0&& arr[j]>key){
                arr[j+1]=arr[j];
                j--;
            }
            arr[j+1]=key;
        }
    }
}
