/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.datastructure;

import lombok.ToString;

/**
 *
 * @author a.kethudaoglu
 */
@ToString
public class BinaryTree {

    Node root;

    private Node addRecursive(Node current, int value) {
        if (current == null) {
            return new Node(value);
        }
        if (value < current.value) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        }
        return current;
    }

    public void add(int value) {
        root = addRecursive(root, value);
    }

    private boolean containsNodeRecursive(Node current, int value) {
        if (current == null) {
            return false;

        }
        if (value == current.value) {
            return true;
        }
        return value < current.value ? containsNodeRecursive(current.left, value) : containsNodeRecursive(current.right, value);

    }

    public boolean containsNode(int value) {
        return containsNodeRecursive(root, value);
    }

    private Node deleteNode(Node current, int value) {
        if (current == null) {
            return null;
        }
        if (current.value == value) {

            if (current.left == null && current.right == null) {
                return null;
            }
            if (current.left == null) {
                return current.right;
            }
            if (current.right == null) {
                return current.left;
            }
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteNode(current.right, smallestValue);
            return current;
        }
        if (value < current.value) {
            current.left = deleteNode(current.left, value);
        } else {
            current.right = deleteNode(current.right, value);
        }
        return current;
    }

    private int findSmallestValue(Node root) {

        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    public void delete(int value) {
        root = deleteNode(root, value);

    }

    public void traverseRecursivePreOrder(Node node) {
        if (node != null) {
            visit(node);
            traverseRecursivePreOrder(node.left);
            traverseRecursivePreOrder(node.right);

        }

    }

    private void visit(Node node) {
        System.out.println(node.value);
    }

    public Node search(Node current, int value) {
        if (current == null || current.value == value) {
            return current;
        }
        if (value < current.value) {
            return search(current.left, value);
        }

        return search(current.right, value);

    }

}
