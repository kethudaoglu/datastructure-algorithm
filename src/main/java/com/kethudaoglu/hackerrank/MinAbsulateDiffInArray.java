/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.Arrays;

/**
 *
 * @author alperen
 */
public class MinAbsulateDiffInArray {
    public static void main(String[] args) {
        int data[]=new int[]{1,5,4,78,43};
        System.out.println(minAbsulateDiffArray(data));
    }
    
    public static int minAbsulateDiffArray(int arr[]){
        var minVal=Integer.MAX_VALUE;
        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            var temp = arr[i];
            minVal=Math.min(minVal, arr[i]);
        }
        return minVal;
    }
}
