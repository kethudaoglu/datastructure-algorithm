/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class LeftRotation {

    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 5, 6};
        var result = leftRotation(arr, 2);
        for (int i : result) {
            System.out.print(i);
        }
    }

    public static int[] leftRotation(int arr[], int rotateSize) {
        final int SIZE = arr.length;
        int rotated[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            int index = i - rotateSize;
            index = index < 0 ? SIZE + index : index;
            rotated[index] = arr[i];
        }

        return rotated;
    }

}
