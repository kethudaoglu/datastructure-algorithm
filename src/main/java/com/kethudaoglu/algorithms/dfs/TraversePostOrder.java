/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.dfs;

/**
 *
 * @author a.kethudaoglu
 */
public class TraversePostOrder {

    public static void main(String[] args) {
     
        new TraversePostOrder().traversePostOrder(SampleData.getSampleData());

    }

    public void traversePostOrder(Node node) {
        if (node != null) {

            traversePostOrder(node.left);
            traversePostOrder(node.right);
            visit(node.value);
        }
    }

    private void visit(int value) {
        System.out.println(value);
    }
}
