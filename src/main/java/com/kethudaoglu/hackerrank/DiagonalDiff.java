/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

/**
 *
 * @author alperen
 */
public class DiagonalDiff {
    public static void main(String[] args) {
        int arr[][]=new int[3][3];
        arr[0][0]=1;
        arr[1][1]=2;
        arr[2][2]=4;
        arr[0][2]=1;
        arr[1][1]=2;
        arr[2][0]=5;
        System.out.println(diagonalDiff(arr));
    }
    
    public static int diagonalDiff(int square[][]){
        int sum=0;
        int temp=0;
        final int SIZE=square.length-1;
        for (int i = 0; i <= SIZE; i++) {
          sum+=square[i][i];
          temp=SIZE-i;
          sum-=square[i][temp];
            
        }
        
        return Math.abs(sum);
    }
}
