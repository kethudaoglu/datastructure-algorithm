/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

/**
 *
 * @author a.kethudaoglu
 * 
 * Sometimes referred to as sinling sort, is a simple sorting algorithm that reapatedly steps through the list,
 * compares adjecent elements and swaps them if they are wrong order.
 * he pass through the list is repeated until the list is sorted.
 * The algorithm, which is a comparison sort, is named for the way smaller or larger elements "bubble" to the top of the list.
 * This simple algorithm performs poorly in real world use and is used primarily as an educational tool.
 * More efficient algorithms such as quicksort, timsort, or merge sort are used by the sorting libraries built into popular programming languages such as Python and Java.
 * 7wikipedia :https://en.wikipedia.org/wiki/Bubble_sort
 * worst-case Q(n2)
 * avarage-case is Q(n2) 
 * Best case Q(n)
 * Q(1) swaps wors case space complexity Q(n) total
 */
public class BubbleSort {

    public static void bubbleSort(int arr[]) {

        if (arr == null || arr.length <= 1) {
            return;
        }

        int i = 0, j = 0, temp = 0;
        boolean swapped = false;
        for (i = 0; i < arr.length-1; i++) {
            swapped=false;
            for ( j = 0; j < arr.length-i-1; j++) {
                if (arr[j]>arr[j+1]){
                    temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                    swapped=true;
                }
            }
            if (!swapped){
                break;
            }
        }

    }
}
