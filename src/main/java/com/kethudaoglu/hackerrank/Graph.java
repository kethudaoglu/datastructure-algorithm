/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 *
 * @author alperen
 */
public class Graph {
    
    private HashMap<Integer, Node> nodeLookup = new HashMap<Integer, Node>();
    
    public static class Node {
        
        private int id;
        LinkedList<Node> adjecent = new LinkedList<>();

        public Node(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Node{" + "id=" + id + '}';
        }
        
    }
    
    private Node getNode(int id) {
        return nodeLookup.get(id);
    }

    public void addNode(int s) {
        nodeLookup.putIfAbsent(s, new Node(s));
    }

    public void addEdge(int source, int destination) {
        
        Node s = getNode(source);
        
        Node d = getNode(destination);
        s.adjecent.add(d);
    }
    
    public boolean hasPathDFS(int source, int destination) {
        Node s = getNode(source);
        Node d = getNode(destination);
        HashSet<Integer> visited = new HashSet<>();
        return hasPathDFS(s, d, visited);
    }
    
    private boolean hasPathDFS(Node source, Node destination, Set visited) {
        
        if (visited.contains(source.id)) {
            return false;
        }
        
        visited.add(source.id);
        if (source.id == destination.id) {
            return true;
        }
        
        for (Node child : source.adjecent) {
            if (hasPathDFS(child, destination, visited)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean hasPathBFS(int source, int destination) {
        return hasPathBFS(getNode(source), getNode(destination));
    }

    private boolean hasPathBFS(Node source, Node destinaion) {
        LinkedList<Node> nextToVisit = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();
        
        nextToVisit.add(source);
        Set<Node> path = new LinkedHashSet<>();
        while (!nextToVisit.isEmpty()) {
            Node node = nextToVisit.remove();
            
           
            if (node.id == destinaion.id) {
                path.forEach(System.out::print);
                return true;
            }
            if (visited.contains(node.id)) {
                continue;
            }
            visited.add(node.id);
           
            for (Node n : node.adjecent) {
                nextToVisit.add(n);
               //path.add(node);
            }
            
        }
        return false;
    }
    
}
