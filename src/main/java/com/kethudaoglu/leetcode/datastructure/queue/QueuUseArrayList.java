/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.leetcode.datastructure.queue;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alperen
 */
public class QueuUseArrayList {
    
}

class Queue{
    List<Integer> data;
    //allways show us head
    int pointer=0;
    Queue(){
        data=new ArrayList<>();
    }
    //enqueue to the queue
    boolean enQueue(Integer value){
        return data.add(value);
    }
    //remove first element from queue
    boolean deQueue(){
        if (pointer>data.size()){
            return false;
        }
   
        pointer++;
        return true;
    }
    
    int front(){
        return data.get(pointer);
    }
}