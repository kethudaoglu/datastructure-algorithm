/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author a.kethudaoglu
 *
 * worst-case Q(n2) worst-case-space Q(n.k)
 *
 * Bucket sort is mainly useful when input is uniformly distributed over a
 * range. For example, consider the following problem. Sort a large set of
 * floating point numbers which are in range from 0.0 to 1.0 and are uniformly
 * distributed across the range. How do we sort the numbers efficiently? A
 * simple way is to apply a comparison based sorting algorithm. The lower bound
 * for Comparison based sorting algorithm (Merge Sort, Heap Sort, Quick-Sort ..
 * etc) is Ω(n Log n), i.e., they cannot do better than nLogn.
 */
public class BucketSort {

    public static void bucketSort(float arr[]) {
        if (arr == null || arr.length < 2) {
            return;
        }
        int n = arr.length;
        //Create new empty buckets
        @SuppressWarnings("unchecked")
        Vector<Float>[] buckets = new Vector[n];

        for (int i = 0; i < n; i++) {
            buckets[i] = new Vector<Float>();

        }

        //put array elements in different buckets;
        for (int i = 0; i < n; i++) {
            float idx = arr[i] * n;
            buckets[(int) idx].add(arr[i]);
        }
        //sort invidual buckets
        for (int i = 0; i < n; i++) {
            Collections.sort(buckets[i]);

        }
        //concanate all buckets into arr
        int index = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < buckets[i].size(); j++) {
                arr[index++] = buckets[i].get(j);
            }
        }
    }
}
