/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.dfs;

import java.util.Stack;

/**
 *
 * @author a.kethudaoglu
 */

public class TraverseInOrder {

    public static void main(String[] args) {
   
        new TraverseInOrder().traverseInOrder(SampleData.getSampleData());
        new TraverseInOrder().traverseInOrderWithoutRecursion(SampleData.getSampleData());
        
    }
 public void traverseInOrderWithoutRecursion(Node root){
     Stack<Node> stack=new Stack<>();
     Node current=root;
     stack.push(root);
     while(!stack.isEmpty()){
         while (current.left!=null){
             current=current.left;
             stack.push(current);
         }
         current=stack.pop();
         visit(current.value);
         if (current.right!=null){
             current=current.right;
             stack.push(current);
         }
     }
     
     
 }
    public void traverseInOrder(Node node) {
        if (node != null) {
         
            traverseInOrder(node.left);
               visit(node.value);
            traverseInOrder(node.right);
        }
    }

    private void visit(int value) {
        System.out.println(value);
    }
}


