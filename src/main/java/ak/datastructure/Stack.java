/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.datastructure;

/**
 *
 * @author alperen
 */
public class Stack {
    
    
    private static class Node{
        private Node next;
        private int data;

        public Node(int data) {
            this.data=data;
        }
        
    }
    
    private Node top;
    
    public boolean isEmpty(){return top==null;}
    public int peek(){
        return top.data;
    }
    
    public void push(int data){
        Node node =new Node(data);
        node.next=top;
        top=node;
    }
    public int pop(){
        int data=top.data;
        top=top.next;
        return data;
    }        
}
