
import com.kethudaoglu.algorithms.graph.Graph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a.kethudaoglu
 */
public class TestGraph {
    
    Graph<String> graph=null;
    
    @Before
    public  void init(){
        graph=new Graph<>();
        graph.addVertex("Ankara");
        graph.addVertex("Konya");
        graph.addVertex("Istanbul");
        graph.addVertex("Çankırı");
        graph.addVertex("Kayseri");
        
        graph.addEdge("Ankara", "Konya", true);
        graph.addEdge("Ankara", "Çankırı", true);
        graph.addEdge("Ankara", "Kayseri", true);
        
    }
    
    @Test
    public void givenGraph_whenCheckVertexSize_thenGetTrueSize(){
        
        Assert.assertEquals(5,graph.getVertexCount());
    }
    
    @Test
    public void givenGraph_whenCheckEdge_thenGetTrueSize(){
        Assert.assertEquals(6, graph.getEdgesCount(false));
    }
    
    @Test
    public void givenGraph_whenCheckHasVertex_thenTrue(){
        Assert.assertTrue(graph.hasVertex("Ankara"));
    }
    
    @Test
    public void giveGraph_whenCheckHasEdge_thenTrue(){
        Assert.assertTrue(graph.hasEdge("Ankara", "Konya"));
    }
    
    @Test
    public void givenGraph_whenBFS_thenTravellAllNode(){
        Graph<Integer> g = new Graph<>(); 
         g.addEdge(0, 1,false); 
        g.addEdge(0, 2,false); 
        g.addEdge(1, 2,false); 
        g.addEdge(2, 0,false); 
        g.addEdge(2, 3,false); 
        g.addEdge(3, 3,false);
        
        g.bfs(2);
        
    
    }
            
}
