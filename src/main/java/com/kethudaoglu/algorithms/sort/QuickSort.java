/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

/**
 * avarage O(nlogn)
 * worst case O (n2)
 * @author a.kethudaoglu
 */
public class QuickSort {

    public static void quickSort(int arr[], int begin, int end) {
        if (begin < end) {
            int partitioningIndex = partition(arr, begin, end);
            quickSort(arr, begin, partitioningIndex - 1);
            quickSort(arr, partitioningIndex + 1, end);

        }

    }

    private static int partition(int[] arr, int begin, int end) {
        int pivot = arr[end];
        int i = (begin - 1);
        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;
                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }

        }

        int swampTemp = arr[i + 1];
        arr[i + 1] = arr[end];
        arr[end] = swampTemp;
        return i + 1;
    }
}
