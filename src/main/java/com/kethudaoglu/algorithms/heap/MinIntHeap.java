/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.algorithms.heap;

import java.util.Arrays;

/**
 *
 * @author alperen
 */
public class MinIntHeap {
  private int capacity;
  private int size;
  int [] items=new int[capacity];
  
  
  public int peek(){
      if (size==0) throw new IllegalStateException();
      return items[0];
      
  }
  public int pop(){
      if (size==0) throw new IllegalStateException();
      int item=items[0];
      items[0]=items[size-1];
      size--;
      heapifyDown();
      return item;
      
  }
  public void add(int item){
      ensureExtraCapacity();
      items[size]=item;
      size++;
      heapifyUp();
  }
  
  private void swap(int indexOne, int indexTwo){
      int temp=items[indexOne];
      items[indexOne]=items[indexTwo];
      items[indexTwo]=temp;
  }
  
  private void ensureExtraCapacity(){
      if (size==capacity){
          items=Arrays.copyOf(items, capacity*2);
          capacity*=2;
      }
  }
  
  
    private void heapifyDown() {
        int index=0;
        }

    private void heapifyUp() {
        
        int index=size-1;
        if (hasParent(index)&& parent(index-1)>items[index]){
            swap(getParentIndex(index), index);
            index=getParentIndex(index);
        }
    }
  
  private int getLeftChildIndex(int parentIndex){
      return 2*parentIndex+1;
  }
  private int getRightChildIndex(int parentIndex){
      return 2*parentIndex+2;
  }        
  private int getParentIndex(int childIndex){
      return (childIndex-1)/2;
  }
  private boolean hasLeftChild(int index){
      return getLeftChildIndex(index)<index;
      
  }
  private boolean hasRightIndex(int index){
      return getRightChildIndex(index)<index;
  }
  private boolean hasParent(int index){
      return getParentIndex(index)>=0;
  }
  private int leftChild(int index){
      return items[getLeftChildIndex(index)];
  }
  private int rightChild(int index){
      return items[getRightChildIndex(index)];
  }
  private int parent(int index){
      return items[getParentIndex(index)];
  }

         
}
