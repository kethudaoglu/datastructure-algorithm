package com.kethudaoglu.hackerrank;


import java.util.Arrays;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author a.kethudaoglu
 */
public class SalesByMatch {

    public static void main(String[] args) {
       int []ar={10,20,20,10,10,30,50,10,20};
        System.out.println(sockMerchant(9, ar));
    }

    static int sockMerchant(int n, int[] ar) {
        return Arrays
                .stream(ar)
                .boxed()
                .collect(Collectors.toList())
                .stream()
                .collect(Collectors.toMap(w->w,w->1, Integer::sum))
                .values().stream()
                .reduce(0,(a,b)->a+b/2, Integer::sum);     

    }
}
