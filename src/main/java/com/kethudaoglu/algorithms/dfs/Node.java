/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.dfs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author a.kethudaoglu
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

class Node {

    Node left;
    Node right;
    int value;
    boolean isVisit=false;

    public Node(int value) {
        this.value = value;
    }

}

