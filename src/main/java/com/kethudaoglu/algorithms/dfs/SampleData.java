/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.dfs;

/**
 *
 * @author a.kethudaoglu
 */
public class SampleData {

    public static Node getSampleData() {
        Node root = new Node(20);
        root.setLeft(new Node(10));
        root.setRight(new Node(21));
        root.getLeft().setLeft(new Node(8));

        root.getLeft().setRight(new Node(12));
        return root;
    }
}
