/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.datastructure;

/**
 *
 * @author alperen
 */
public class Queue {

    private static class Node {

        private Node next;
        private int data;

        public Node(int data) {
            this.data = data;
        }

    }

    private Node head; // remove from the head
    private Node tail; // add things here

    private Boolean isEmpty() {
        return head == null;
    }

    public int peak() {
        return head.data;

    }

    public void add(int value) {
        Node node = new Node(value);
        if (tail != null) {
            tail.next = node;
        }
        tail = node;

        if (head == null) {
            head = node;
        }

    }
    
    public int remove(){
        int data=head.data;
        head=head.next;
        if (head==null){
            tail=null;
        }
        return data;
    }

}
