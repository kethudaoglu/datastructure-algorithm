/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.graph.hackerrank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author a.kethudaoglu
 */
public class Graph {
    private HashMap<Integer,Node> nodeLookup=new HashMap<>();
    
    public static class Node{
        private int id;
        LinkedList<Node> adjacent=new LinkedList<>();
        private Node(int id){
            this.id=id;
        }
    }
    
    private Node getNode(int id){
        return nodeLookup.get(id);
    }
    
    public void addEdge(int source, int destination){
        getNode(source).adjacent.add(getNode(destination));
    }
    public boolean hasPathDFS(int source, int destination){
        HashSet<Integer> visited=new HashSet<Integer>();
        return hasPathDFS(getNode(source),getNode(destination),visited);
                
    }
    private boolean hasPathDFS(Node source, Node destination, HashSet<Integer> visited){
        if (visited.contains(source)){
            return false;
        }
       visited.add(source.id);
       if (source==destination){
           return true;
       }
       for (Node child: source.adjacent){
           if (hasPathDFS(child, destination,visited)){
               return true;
           }
       }
       return false;
    }
    public boolean hasPathBFS(int source,int destination){
        return hasPathBFS(getNode(source), getNode(destination));
    }
    private boolean hasPathBFS(Node source,Node destination){
        LinkedList<Node> nextToVisit=new LinkedList<>();
        HashSet<Integer> visited=new HashSet<Integer>();
        nextToVisit.add(source);
        while (!nextToVisit.isEmpty()){
            Node node=nextToVisit.remove();
            if (node==destination){
                return true;
            }
            if (visited.contains(node.id)){
                continue;
            }
            visited.add(node.id);
            for (Node child: node.adjacent){
                nextToVisit.add(child);
            }
        }
        return false;
    }
}
