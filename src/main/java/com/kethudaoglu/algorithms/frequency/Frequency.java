/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.frequency;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author a.kethudaoglu
 */
public class Frequency<V>{
    
    public  <V> Map<V,Long> calculateFrequency(final Collection<V> items){
         return items.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));   
    }
    
    public  Map calculateFrequencyOtherWay(final Collection<V> items){
        return items.stream().collect(Collectors.toMap(w->w,w->1L,Long::sum));
    }
    //to get inserted order
      public Map calcualteFrequencyLinkedHashMap( final Collection<V> items){
        return items.stream().collect(Collectors.toMap(w->w,w->1L,Long::sum,LinkedHashMap::new ));
    }
}
