
import com.kethudaoglu.algorithms.frequency.Frequency;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author a.kethudaoglu
 */
public class TestFrequency {

    Frequency<String> f = new Frequency<>();
    Frequency<Person>  personsF=new Frequency<>();
    List<String> list = new ArrayList<>();
    List<Person> persons=new ArrayList<>();
    Map<Person,Long> exptectedPerson=new HashMap<>();
    
    Map<String, Long> expected = new HashMap<>();
    

    @Before
    public void init() {
        list.add("Alperen");
        list.add("Alperen");
        list.add("Eren");
        
        persons.add(new Person("Alperen", "Kethudaoğlu", 22));
        persons.add(new Person("Alperen", "Kethudaoğlu", 22));
        persons.add(new Person("Fatma", "Kethudaoğlu", 21));
        expected.put("Alperen", 2L);
        expected.put("Eren", 1L);
        
        
        exptectedPerson.put(new Person("Alperen", "Kethudaoğlu", 22), 2L);
        exptectedPerson.put(new Person("Fatma", "Kethudaoğlu", 21), 1L);
    }

    @Test
    public void givenFrequency_whenCollections_thenGetFrequencyAsMap() {

        Assert.assertTrue(expected.equals(f.calculateFrequency(list)));
    }

    @Test
    public void givenFrequency_whenCollectionsWithOtherMetod_thenGetFrequencyAsMap() {
        Assert.assertTrue(expected.equals(f.calculateFrequencyOtherWay(list)));
    }
    
    @Test
    public void givenFrequency_whenCollectionsPerson_thenGetFrequencyAsmap(){
        Assert.assertTrue(exptectedPerson.equals(personsF.calculateFrequency(persons)));
    }
    
    @Test
    public void givenFrequency_whenCollectionsPersonWithOtherMerhod_thenGetFrequencyAsmap(){
        Assert.assertTrue(exptectedPerson.equals(personsF.calculateFrequencyOtherWay(persons)));
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    class Person{
       private  String name;
       private String surname;
       private Integer age;
        
    }
            
}
