
import com.kethudaoglu.algorithms.sort.BubbleSort;
import com.kethudaoglu.algorithms.sort.BucketSort;
import com.kethudaoglu.algorithms.sort.MergeSort;
import com.kethudaoglu.algorithms.sort.QuickSort;
import com.kethudaoglu.algorithms.sort.RadixSort;
import com.kethudaoglu.algorithms.sort.Utils;
import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author a.kethudaoglu
 */
public class TestSortAlgorithms {

    @Test
    public void givenIntArray_whenSortetWithBubbleSort_thenSorted() {
        int actual[] = {1, 45, 8, 87, 4};
        int expectedArr[] = {1, 4, 8, 45, 87};
        BubbleSort.bubbleSort(actual);
        Assert.assertArrayEquals(expectedArr, actual);
    }

    @Test
    public void givenIntArray_whenSortedwithMergeSort_thenSortedArray() {
        int[] actual = {5, 1, 6, 2, 3, 4};
        int[] expected = {1, 2, 3, 4, 5, 6};
        MergeSort.mergeSort(actual, actual.length);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void givenIntArray_whenRadixSort_thenSortedArray() {
        int[] actual = {45, 1, 67, 2, 3, 4};
        int[] expected = {1, 2, 3, 4, 45, 67};
        RadixSort.radixSort(actual);
        Assert.assertArrayEquals(expected, actual);

    }

    @Test
    public void givenFloatArray_whenSortedwithBucketSort_thenSortedArray() {
        float[] actual = {0.45f, 0.1f, 0.67f, 0.2f, 0.3f, 0.4f};
        float[] expected = {0.1f, 0.2f, 0.3f, 0.4f, 0.45f, 0.67f};
        BucketSort.bucketSort(actual);
        Assert.assertArrayEquals(expected, actual, 2);
    }
    
    @Test
    public void givenIntArray_whenSortedWithQuickSort_thenSortedArray(){
               int[] actual = {45, 1, 67, 2, 3, 4};
        int[] expected = {1, 2, 3, 4, 45, 67};
        QuickSort.quickSort(actual,0,actual.length-1);
        Utils.printArray(actual);
        Assert.assertArrayEquals(expected, actual);
    }
            

}
