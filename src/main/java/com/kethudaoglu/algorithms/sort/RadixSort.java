/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.sort;

import java.util.Arrays;
import java.util.stream.IntStream;


 
    /**
     * Radix sort is a sorting algorithm that sorts numbers based on the
     * positions of their digits. Basically, it uses the place value of the
     * digits in a number.
     *
     * Unlike most of the other sorting algorithms, such as Merge Sort,
     * Insertion Sort, Bubble sort, it doesn't compare the numbers
     *
     * Radix sort works by sorting digits form the LEast Significant Digit (LSD)
     * to the Most Significant Digit(MSd).
     *
     * From MSD;
     */
public class RadixSort {

    public static void radixSort(int arr[]) {
        int numberOfDigits = calculateDigitsOfMaxNumber(arr);
        int placeValue=1;
        while (numberOfDigits-->0){
            applyCountingSort(arr, placeValue);
            placeValue*=10;
        }
    }

    private static int calculateDigitsOfMaxNumber(int[] arr) {
        int maxNumber = IntStream.of(arr).max().orElse(0);
        int digitSize = 0;
        while (maxNumber != 0) {
            maxNumber /= 10;
            digitSize++;
        }
        return digitSize;
    }

    private static void applyCountingSort(int[] arr, int placeValue) {
        int output[] = new int[arr.length];
        int range = 10;
        int count[] = new int[10];
        Arrays.fill(count, 0);
        //store count of occurinces in cpunt
        for (int i = 0; i < arr.length; i++) {
            count[(arr[i] / placeValue) % 10]++;
        }

//        Utils.printArray(count);

        //Change count[i] so that count[i] now contains
        //actual positiion of theis digit in output
        for (int i = 1; i < 10; i++) {
            count[i] += count[i - 1];
        }

        //build to output array
        for (int i = arr.length - 1; i >= 0; i--) {
            output[count[(arr[i] / placeValue) % 10] - 1] = arr[i];
            count[(arr[i] / placeValue) % 10]--;
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i] = output[i];
        }
    }
}
