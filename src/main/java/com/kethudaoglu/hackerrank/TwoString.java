/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author alperen
 */
public class TwoString {
    public static void main(String[] args) {
        System.out.println(twoString("alperen", "omaz"));
    }
    public static boolean twoString(String val1,String val2){
        Set<Character> set=new HashSet<>();
        char []value1=val1.toCharArray();
        for (int i = 0; i < value1.length; i++) {
            set.add(value1[i]);
            
        }
        
        for (int i = 0; i < val2.length(); i++) {
            char c = val2.charAt(i);
            if (set.contains(c)){
                return true;
            }
            
        }
        return false;
    }
}
