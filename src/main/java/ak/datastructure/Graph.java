/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ak.datastructure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


/**
 *
 * @author alperen
 */
public class Graph {
    HashMap<Integer,Node> lookup=new HashMap<>();
      public static class Node{
          int id;
          LinkedList<Node> adjecent=new LinkedList<>();
          public Node(int id){
              this.id=id;
          }
          @Override
          public String toString(){
              return String.valueOf(id);
          }
      }
      
      public void addNode(int id){
          lookup.putIfAbsent(id, new Node(id));
      }
      public Node getNode(int id){
         return lookup.get(id);
      }
      public void addEdge(int source, int destination){
          Node s=getNode(source);
          Node d=getNode(destination);
          s.adjecent.add(d);
      }
      
      public boolean hasPathDFS(int source, int destination){
          Node s=getNode(source);
          Node d=getNode(destination);
          HashSet<Integer> visited=new HashSet<>();
         
          return hasPathDFS(s,d,visited);
      }
      
      private boolean hasPathDFS(Node source, Node destination, Set visited){
          if (visited.contains(source.id)){
              return false;
          }
          visited.add(source.id);
          if (source==destination){
              return true;
          }
          for (Node node : source.adjecent) {
              if (hasPathDFS(node, destination, visited)){
                  return true;
              }
          }
        
          return false;
      }
      
      public boolean hasPathBFS(int source, int destination){
          return hasPathBFS(getNode(source),getNode(destination));
      }
      
      private boolean hasPathBFS(Node source, Node destination){
          LinkedList<Node> nextToVisit=new LinkedList<>();
          Set<Integer> visited=new HashSet<>();
          nextToVisit.add(source);
          while (!nextToVisit.isEmpty()){
              Node node=nextToVisit.remove();
              if (visited.contains(node.id)){
                  continue;
              }
              if (node.id==destination.id){
     
                  return true;
              }
              visited.add(node.id);
              for (Node child : node.adjecent) {
                  nextToVisit.add(child);
              }
          }
          return false;
      }
}
