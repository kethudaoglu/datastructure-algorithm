/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.graph;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Graph class is implemented using HashMap
 *
 * @author a.kethudaoglu
 * @param <T>
 */
public class Graph<T> {

    public final Map<T, LinkedList<T>> map = new LinkedHashMap<>();
    public final Stack<Integer> stack = new Stack<>();

    public void addVertex(T t) {
        map.put(t, new LinkedList<>());
    }

    public void addEdge(T source, T destination, boolean bidirectional) {
        if (!map.containsKey(source)) {
            addVertex(source);
        }
        if (!map.containsKey(destination)) {
            addVertex(destination);
        }

        map.get(source).add(destination);
        if (bidirectional) {
            map.get(destination).add(source);
        }

    }

    public int getVertexCount() {
        return map.keySet().size();
    }

    public long getEdgesCount(boolean bidirectional) {
        long count = map.values().stream().flatMap(Collection::stream).count();
        if (bidirectional) {
            count /= 2;
        }
        return count;
    }
    public Set<Integer> listOfUniqueEdge(){
        return (Set<Integer>)map.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
    }      

    public boolean hasVertex(T t) {
        return map.containsKey(t);
    }

    public boolean hasEdge(T source, T destination) {
        return map.get(source).contains(destination);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (T v : map.keySet()) {
            builder.append(v.toString() + ": ");
            for (T w : map.get(v)) {
                builder.append(w.toString() + " ");
            }
            builder.append("\n");
        }

        return (builder.toString());
    }

    public void dfs(int vertex) {
        boolean visited[] = new boolean[this.map.size()];

        Iterator<T> itr = map.get(vertex).iterator();
        while (itr.hasNext()) {
            int n = (int) itr.next();
            dfsUtil(n, visited);
        }
    }

    private void dfsUtil(int v, boolean visited[]) {
        visited[v] = true;
        stack.add(v);
        Iterator itr = map.get(v).iterator();
        while (itr.hasNext()) {
            int n = (int) itr.next();
            if (!visited[n]) {
                dfsUtil(n, visited);
            }
        }
    }
//it uses to find shortest path.
    public void bfs(Integer v) {
        //Mark all the vertices as not visited[by default set as false]
        boolean visited[] = new boolean[map.size()];
        //Create a queue for BFS
        LinkedList<Integer> queue = new LinkedList<>();

        //Mark the current node as visited and enqueue it
        visited[v] = true;
        queue.add(v);
        while (queue.size() != 0) {
            //dequeue a vertex from queue and print it
            v=queue.poll();
            System.out.println("s "+v);
            //get all adjecent vertices of the dequeued vertex s
            //if a adjacent has not been visited, hen mark it visited and enqueue it
            Iterator<T> itr = map.get(v).iterator();
            while (itr.hasNext()){
                int n=(int)itr.next();
                if (!visited[n]){
                    visited[n]=true;
                    queue.add(n);
                }
            }
        }
        

    }
    
    //it uses to find shortest path.
    public void bfsForShortest(Integer v, Integer targetNode) {
        //Mark all the vertices as not visited[by default set as false]
        boolean visited[] = new boolean[map.size()];
        //Create a queue for BFS
        LinkedList<Integer> queue = new LinkedList<>();

        //Mark the current node as visited and enqueue it
        visited[v] = true;
        queue.add(v);
        while (queue.size() != 0) {
            //dequeue a vertex from queue and print it
            v=queue.poll();
            System.out.println("s "+v);
            //get all adjecent vertices of the dequeued vertex s
            //if a adjacent has not been visited, hen mark it visited and enqueue it
            Iterator<T> itr = map.get(v).iterator();
            while (itr.hasNext()){
                int n=(int)itr.next();
                if (!visited[n]){
                    visited[n]=true;
                    queue.add(n);
                }
            }
        }
        

    }
}
