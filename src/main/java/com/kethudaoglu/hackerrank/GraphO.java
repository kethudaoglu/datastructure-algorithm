/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author alperen
 */
public class GraphO {

    private HashMap<Integer,Node> nodeLookup=new HashMap<>();
    
    public static class Node{
        private int id ;
        LinkedList<Node> adjecent=new LinkedList<>();
        public Node(int id){
            this.id=id;
        }

        @Override
        public String toString() {
            return "Node{" + "id=" + id + '}';
        }
        
        
    }
    
    private Node getNode(Integer id){
        return nodeLookup.get(id);
    }
    public void addNode(Integer id){
        nodeLookup.putIfAbsent(id, new Node(id));
    }
    public void addEdge(int source,int dest){
        var s=getNode(source);
        var d=getNode(dest);
        s.adjecent.add(d);
    }
    public LinkedList<Node> breadFirstSearch(int source, int dest){
        LinkedList<Node> nextToVisit=new LinkedList<>();
        Set<Node> visited=new HashSet<>();
        Map<Node,Node> parent=new LinkedHashMap<>();
        LinkedList<Node> path=new LinkedList<Node>();
        Node s=getNode(source);
        Node d=getNode(dest);
        visited.add(s);
        nextToVisit.add(s);
        parent.put(s,s);
        while (!nextToVisit.isEmpty()){
            Node current=nextToVisit.poll();
            if (current==d){
                break;
            }
            for (Node neighbour:current.adjecent){
                if (!visited.contains(neighbour)){
                    
                    visited.add(neighbour);
                    nextToVisit.add(neighbour);
                    parent.put(neighbour,current);
                }
            }
            
        }
        Node curr=d;
        while (curr!=s){
           curr= parent.get(curr);
           path.push(curr);
        }
        
       
      //  System.out.println(parent);
        return path;
    }
}
