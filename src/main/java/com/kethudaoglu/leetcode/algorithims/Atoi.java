/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.leetcode.algorithims;

/**
 *
 * @author alperen
 */
public class Atoi {

    public static void main(String[] args) {
        System.out.printf("%f%n",Math.pow(2, 32));
            String vallue="-123";
            System.out.println(atoi(vallue));
    }

    public static int atoi(String value) {
        int sign = 1;

        if (value != null && !value.isBlank()) {
            sign = value.charAt(0) == '-' ? -1 : sign;
            

        }
        int index = sign == -1 ? 1 : 0;
        int result = 0;
        for (; index < value.length(); index++) {
            result = result * 10 + value.charAt(index) - '0';
            
        }
        int min=-2147483648;
        int max=2147483647;
        result=result>max?max:result;
        result=result<min?min:result;
        return sign * result;
    }
}
