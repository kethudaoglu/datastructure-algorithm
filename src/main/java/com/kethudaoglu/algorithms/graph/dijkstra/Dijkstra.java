/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.graph.dijkstra;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author a.kethudaoglu
 */
public class Dijkstra {
    
    public static Graph calculateShortestPath(Graph graph, Node source){
        source.distance=0;
        Set<Node> settleNodes=new HashSet<>();
        Set<Node> unSettledNode=new HashSet<>();
        
        unSettledNode.add(source);
        while (!unSettledNode.isEmpty()){
            Node currentNode=getLowestDistanceNode(unSettledNode);
            unSettledNode.remove(source);
            for (Map.Entry<Node, Integer> adjacencyPair:currentNode.adjacentNodes.entrySet()){
                Node adjacentNode=adjacencyPair.getKey();
                Integer edgeWeigh=adjacencyPair.getValue();
                
                if (!settleNodes.contains(adjacentNode)){
                    calculateMinimumDistance(adjacentNode,edgeWeigh,currentNode);
                    unSettledNode.add(adjacentNode);
                }
            }
            settleNodes.add(currentNode);
        }
        return graph;
    }

    private static Node getLowestDistanceNode(Set<Node> unSettledNode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void calculateMinimumDistance(Node adjacentNode, Integer edgeWeigh, Node currentNode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
             
}
