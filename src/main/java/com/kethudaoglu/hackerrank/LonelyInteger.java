/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author alperen
 */
public class LonelyInteger {
    public static void main(String[] args) {
       int data[]={1,1,2,3,2,3,5,7,7};
        System.out.println(lonelyInteger(data));
    }
    
    public static int lonelyInteger(int arr[]){
        int result=0;
        for (int i : arr) {
          result^=i;       
        }
     return  result;
    
    }
}
