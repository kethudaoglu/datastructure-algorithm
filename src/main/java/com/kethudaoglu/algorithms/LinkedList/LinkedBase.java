/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.algorithms.LinkedList;

/**
 *
 * @author a.kethudaoglu
 */
public class LinkedBase {

    private Node root;

    public static class Node {

        private Integer id = null;
        private String data;
        Node next;

        private Node(int id, String data) {
            this.id = id;
            this.data = data;
        }
    }

    public static void main(String[] args) {
        LinkedBase base =new LinkedBase();
        base.add(55, "Ahmet KARA");
        base.add(52, "Fatih KALENDER");
        base.add(59, "Selim AYAZ");
        base.add(12, "Yağmur DERE");
       base.traverse(base.root);
        System.out.println("\n--------------");
        base.find(base.root, 52);
        System.out.println("\n--------------");
        base.remove(base.root, 55);
        base.traverse(base.root);
    }
    
    public void add(int id, String data) {
        //first data
        if (root == null) {
            root=new Node(id,data);
        } else {
        add(this.root, id, data);
        }
    }

    private void add(Node node, Integer id, String data) {
        if (node.id % 10 == id % 10) {
            Node temp = node.next;
            if (node.next != null) {
                add(node.next, id, data);
            } else {
                node.next = new Node(id, data);
                return;
            }

        }
        if (node.id % 10 < id % 10) {
            Node temp = new Node(node.id, node.data);
            temp.next = node.next;
            node.id = id;
            node.data = data;
            node.next = temp;
            return;
        }

        if (node.id % 10 > id%10) {
            if (node.next != null) {
                add(node.next, id, data);
            }else {
                node.next=new Node(id,data);
            }

        }
    }
    
    public void traverse(Node node){
        while (node!=null){
            System.out.printf("%d %s \n",node.id,node.data);
            node=node.next;
        }
    }
    
    public void find(Node node,int id){
        int count=0;
         boolean statu=false;
        while (node!=null){
            System.out.printf("%d %s \n",node.id,node.data);
            
            count++;
            if(node.id==id){
                statu=true;
                break;
            }
            node=node.next;
        }
        if (statu){
            System.out.printf("%d adımda bulunmuştur.",count);
        }else {
            System.out.println("Girilen değer listede yoktur.");
        }
    }
    
    public void remove(Node node,int id){
        while (node!=null){
            
            if (node.next.id==id){
                if (node.next.next==null){
                    node.next=null;
                }else {
                    node.next=node.next.next;
                }
                break;
            }
            node =node.next;
        }
    }
    
}
