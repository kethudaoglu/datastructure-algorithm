/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.datastructure;

/**
 *
 * @author a.kethudaoglu
 */
public class Test {

    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.add(10);
        tree.add(80);
        tree.add(50);
        tree.add(40);
        tree.add(70);
        tree.add(60);
        tree.add(75);
        tree.add(20);
        tree.add(100);
        System.out.println(tree);

        System.out.println(tree.containsNode(80));

        tree.traverseRecursivePreOrder(tree.root);
        System.out.println("-----");
        tree.delete(50);
        System.out.println("-----");
        tree.traverseRecursivePreOrder(tree.root);
        
        Node node=tree.search(tree.root, 40);
        System.out.println("found : \n"+node);
    }
}
