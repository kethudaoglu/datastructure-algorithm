/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kethudaoglu.datastructure;

import lombok.Data;

/**
 *
 * @author a.kethudaoglu
 */

public class Node {
int value;
Node left;
Node right;
Node (int value){
    this.value=value;
    right=null;
    left=null;
}

    @Override
    public String toString() {
       
        StringBuilder sb=new StringBuilder();
        sb.append("current value : "+this.value);
        sb.append("\nleft : ");
        if (left!=null){
            sb.append(left.value);
        }else {
            sb.append("null");
        }
        sb.append("\nright : ");
        if (right!=null){
            sb.append(right.value);
        }else {
            sb.append("null");
        
        }
        return sb.toString();
    }

}
