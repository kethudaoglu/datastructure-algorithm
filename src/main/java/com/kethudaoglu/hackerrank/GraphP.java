/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kethudaoglu.hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author alperen
 */
public class GraphP {

    LinkedList<Integer> adjacencyList[];

    public GraphP(int vertex) {
        adjacencyList = new LinkedList[vertex];
        for (int i = 0; i < adjacencyList.length; i++) {
            adjacencyList[i] = new LinkedList<>();

        }
    }

    public void addEdge(int source, int dest) {
        adjacencyList[source].add(dest);
        adjacencyList[dest].add(source);
    }

    public List<Integer> breadFirstSearch(int source, int dest) {
        boolean visited[] = new boolean[adjacencyList.length];
        int parent[] = new int[adjacencyList.length];
        Queue<Integer> nextToVisit = new LinkedList<>();
        List<Integer> path=new ArrayList<>();

        nextToVisit.add(source);
        parent[source] = -1;
        visited[source] = true;
        while (!nextToVisit.isEmpty()) {
            int current = nextToVisit.poll();
            if (current == dest) {
                break;
            }
            for (Integer neighbour : adjacencyList[current]) {
                if (!visited[neighbour]) {
                    visited[neighbour] = true;
                    nextToVisit.add(neighbour);
                    parent[neighbour] = current;
                }
            }

        }
        //travers through to parent array
        int current = dest;
        int distance=0;
        while (parent[current]!=-1){
            if (!path.contains(dest)){
                path.add(dest);
            }
            current=parent[current];
            path.add(current);
         
            distance++;
                    
                   
        }
     Collections.reverse(path);
         return path;
    }
}
